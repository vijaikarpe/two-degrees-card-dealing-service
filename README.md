**Docker Container Info**

Build Command: docker build -f Dockerfile -t two-degrees-card-dealer-service:0.0.1 .

Run the Image using the port (The port has to be the same as specified in the application's properties)

Run Command: docker run -p 1102:1102 two-degrees-card-dealer-service:0.0.1 

**Push to Docker Hub**
docker tag <image-name> <username>/<repository>:<tag>
docker tag two-degrees-card-dealer-service:0.0.1 karpetec/primezone:two-degrees-card-dealer-service-0.0.1

docker push <username>/<repository>:<tag>
docker push karpetec/primezone:two-degrees-card-dealer-service-0.0.1

**Log files Info**

Windows: The destination folder for the logs will be in the following location in which ever drive the springboot application is run.
 
{Drive}/ServiceLogs/two-degrees-card-dealer-service


Mac / Linux: Change the path to the User's Home Directory _prior to building project_


**Swagger UI**

http://localhost:1102/card-dealer-service/swagger-ui.html

