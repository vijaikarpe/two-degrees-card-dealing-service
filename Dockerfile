#The Docker image, based on the JDK Version
FROM openjdk:11.0.4-slim
# Add the jar, being built by the app, based on the final name in the POM (Path Name of the jar)
ADD target/two-degrees-card-dealer-service.jar two-degrees-card-dealer-service.jar
#Expose the port which must be the same as in the application.yml file
EXPOSE 1100
#Entry point (as to run a jar - add more commands if required)
ENTRYPOINT ["java", "-jar", "two-degrees-card-dealer-service.jar"]
#push to docker hub
