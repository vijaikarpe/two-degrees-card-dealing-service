package two.degrees.nz.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

@ApiModel(description = "A single card from a French deck of Cards.")
public class ShuffledFrenchCardDeck {

    private static final String FIELD_EMPTY_MESSAGE = "Field cannot be empty.";

    @ApiModelProperty(notes = "The Shuffled Deck of Cards.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    private List<Card> shuffledDeck;

    public List<Card> getShuffledDeck() {
        return shuffledDeck;
    }

    public void setShuffledDeck(List<Card> shuffledDeck) {
        this.shuffledDeck = shuffledDeck;
    }

    public ShuffledFrenchCardDeck() {
        shuffledDeck = shuffleDeck();
    }

    public List<Card> shuffleArrayRandomly(List<Card> a) {
        List<Card> b = new ArrayList<>();
        while (a.size() != 0) {
            int arrayIndex = (int) (Math.random() * (a.size()));
            b.add(a.get(arrayIndex));
            a.remove(a.get(arrayIndex));
        }
        return b;
    }

    public List<Card> shuffleDeck(){

        List<Card> existingCardDeck = new ArrayList<>();

        for (Card value : Card.values()) {

            if (value != Card.JOKER) {
                existingCardDeck.add(value);
            }
        }
        return shuffleArrayRandomly(existingCardDeck);
    }

    public enum Card {

        DIAMONDS_ACE(1, SuiteType.DIAMONDS, CardIndicator.ACE),
        DIAMONDS_TWO(2, SuiteType.DIAMONDS, CardIndicator.TWO),
        DIAMONDS_THREE(3, SuiteType.DIAMONDS, CardIndicator.THREE),
        DIAMONDS_FOUR(4, SuiteType.DIAMONDS, CardIndicator.FOUR),
        DIAMONDS_FIVE(5, SuiteType.DIAMONDS, CardIndicator.FIVE),
        DIAMONDS_SIX(6, SuiteType.DIAMONDS, CardIndicator.SIX),
        DIAMONDS_SEVEN(7, SuiteType.DIAMONDS, CardIndicator.SEVEN),
        DIAMONDS_EIGHT(8, SuiteType.DIAMONDS, CardIndicator.EIGHT),
        DIAMONDS_NINE(9, SuiteType.DIAMONDS, CardIndicator.NINE),
        DIAMONDS_TEN(10, SuiteType.DIAMONDS, CardIndicator.TEN),
        DIAMONDS_JACK(11, SuiteType.DIAMONDS, CardIndicator.JACK),
        DIAMONDS_QUEEN(12, SuiteType.DIAMONDS, CardIndicator.QUEEN),
        DIAMONDS_KING(13, SuiteType.DIAMONDS, CardIndicator.KING),

        HEARTS_ACE(1, SuiteType.HEARTS, CardIndicator.ACE),
        HEARTS_TWO(2, SuiteType.HEARTS, CardIndicator.TWO),
        HEARTS_THREE(3, SuiteType.HEARTS, CardIndicator.THREE),
        HEARTS_FOUR(4, SuiteType.HEARTS, CardIndicator.FOUR),
        HEARTS_FIVE(5, SuiteType.HEARTS, CardIndicator.FIVE),
        HEARTS_SIX(6, SuiteType.HEARTS, CardIndicator.SIX),
        HEARTS_SEVEN(7, SuiteType.HEARTS, CardIndicator.SEVEN),
        HEARTS_EIGHT(8, SuiteType.HEARTS, CardIndicator.EIGHT),
        HEARTS_NINE(9, SuiteType.HEARTS, CardIndicator.NINE),
        HEARTS_TEN(10, SuiteType.HEARTS, CardIndicator.TEN),
        HEARTS_JACK(11, SuiteType.HEARTS, CardIndicator.JACK),
        HEARTS_QUEEN(12, SuiteType.HEARTS, CardIndicator.QUEEN),
        HEARTS_KING(13, SuiteType.HEARTS, CardIndicator.KING),

        SPADES_ACE(1, SuiteType.SPADES, CardIndicator.ACE),
        SPADES_TWO(2, SuiteType.SPADES, CardIndicator.TWO),
        SPADES_THREE(3, SuiteType.SPADES, CardIndicator.THREE),
        SPADES_FOUR(4, SuiteType.SPADES, CardIndicator.FOUR),
        SPADES_FIVE(5, SuiteType.SPADES, CardIndicator.FIVE),
        SPADES_SIX(6, SuiteType.SPADES, CardIndicator.SIX),
        SPADES_SEVEN(7, SuiteType.SPADES, CardIndicator.SEVEN),
        SPADES_EIGHT(8, SuiteType.SPADES, CardIndicator.EIGHT),
        SPADES_NINE(9, SuiteType.SPADES, CardIndicator.NINE),
        SPADES_TEN(10, SuiteType.SPADES, CardIndicator.TEN),
        SPADES_JACK(11, SuiteType.SPADES, CardIndicator.JACK),
        SPADES_QUEEN(12, SuiteType.SPADES, CardIndicator.QUEEN),
        SPADES_KING(13, SuiteType.SPADES, CardIndicator.KING),


        CLUBS_ACE(1, SuiteType.CLUBS, CardIndicator.ACE),
        CLUBS_TWO(2, SuiteType.CLUBS, CardIndicator.TWO),
        CLUBS_THREE(3, SuiteType.CLUBS, CardIndicator.THREE),
        CLUBS_FOUR(4, SuiteType.CLUBS, CardIndicator.FOUR),
        CLUBS_FIVE(5, SuiteType.CLUBS, CardIndicator.FIVE),
        CLUBS_SIX(6, SuiteType.CLUBS, CardIndicator.SIX),
        CLUBS_SEVEN(7, SuiteType.CLUBS, CardIndicator.SEVEN),
        CLUBS_EIGHT(8, SuiteType.CLUBS, CardIndicator.EIGHT),
        CLUBS_NINE(9, SuiteType.CLUBS, CardIndicator.NINE),
        CLUBS_TEN(10, SuiteType.CLUBS, CardIndicator.TEN),
        CLUBS_JACK(11, SuiteType.CLUBS, CardIndicator.JACK),
        CLUBS_QUEEN(12, SuiteType.CLUBS, CardIndicator.QUEEN),
        CLUBS_KING(13, SuiteType.CLUBS, CardIndicator.KING),

        JOKER(0, SuiteType.NONE, CardIndicator.JOKER);


        private SuiteType suiteType;
        private Integer cardValue;
        private CardIndicator cardIndicator;

        Card(Integer cardValue, SuiteType suiteType, CardIndicator cardIndicator) {
            this.suiteType = suiteType;
            this.cardValue = cardValue;
            this.cardIndicator = cardIndicator;
        }

        public SuiteType getSuiteType() {
            return suiteType;
        }

        public Integer getCardValue() {
            return cardValue;
        }

        public CardIndicator getCardIndicator() {
            return cardIndicator;
        }

        public Card getRandomCardFrom() {
            Random rnd = ThreadLocalRandom.current();
            return values()[(int) (Math.random() * values().length)];


        }
    }

    public enum SuiteType {
        HEARTS, DIAMONDS, SPADES, CLUBS, NONE
    }

    public enum CardIndicator {
        ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, JOKER
    }
}
