package two.degrees.nz.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.util.List;

@ApiModel(description = "A distributed french deck of cards amongst four players.")
public class DistributedFrenchCardDeck {

    private static final String FIELD_EMPTY_MESSAGE = "players set cannot be empty.";

    @ApiModelProperty(notes = "The shuffled french deck of cards, distributed to player one.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    private PlayerSet playerSetOne;

    @ApiModelProperty(notes = "The shuffled french deck of cards, distributed to player two.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    private PlayerSet playerSetTwo;

    @ApiModelProperty(notes = "The shuffled french deck of cards, distributed to player three.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    private PlayerSet playerSetThree;

    @ApiModelProperty(notes = "The shuffled french deck of cards, distributed to player four.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    private PlayerSet playerSetFour;

    @ApiModelProperty(notes = "The shuffled french deck of cards.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    private List<ShuffledFrenchCardDeck.Card> shuffledDeck;

    public DistributedFrenchCardDeck() {

        PlayerSet playerSet1 = new PlayerSet();
        playerSet1.setPlayerName("");
        this.playerSetOne = playerSet1;

        PlayerSet playerSet2 = new PlayerSet();
        playerSet2.setPlayerName("");
        this.playerSetTwo = playerSet2;

        PlayerSet playerSet3 = new PlayerSet();
        playerSet3.setPlayerName("");
        this.playerSetThree = playerSet3;

        PlayerSet playerSet4 = new PlayerSet();
        playerSet4.setPlayerName("");
        this.playerSetFour = playerSet4;

    }

    public List<ShuffledFrenchCardDeck.Card> getShuffledDeck() {
        return shuffledDeck;
    }

    public void setShuffledDeck(List<ShuffledFrenchCardDeck.Card> shuffledDeck) {
        this.shuffledDeck = shuffledDeck;
    }

    public PlayerSet getPlayerSetOne() {
        return playerSetOne;
    }

    public void setPlayerSetOne(PlayerSet playerSetOne) {
        this.playerSetOne = playerSetOne;
    }

    public PlayerSet getPlayerSetTwo() {
        return playerSetTwo;
    }

    public void setPlayerSetTwo(PlayerSet playerSetTwo) {
        this.playerSetTwo = playerSetTwo;
    }

    public PlayerSet getPlayerSetThree() {
        return playerSetThree;
    }

    public void setPlayerSetThree(PlayerSet playerSetThree) {
        this.playerSetThree = playerSetThree;
    }

    public PlayerSet getPlayerSetFour() {
        return playerSetFour;
    }

    public void setPlayerSetFour(PlayerSet playerSetFour) {
        this.playerSetFour = playerSetFour;
    }
}
