package two.degrees.nz.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.util.List;

@ApiModel(description = "Results after dealing deck of cards amongst four players.")
public class Results {

    public enum WinningAttribute{

        HIGHEST_SCORE ("The win is based on highest score."),
        TIE_KING_OF_CLUBS ("There was a tie, but the winner is based on possesing the king of clubs."),
        TIE_KING_OF_DIAMONDS("There was a tie, but the winner is based on possesing the king of diamonds."),
        TIE_KING_OF_HEARTS("There was a tie, but the winner is based on possesing the king of hearts."),
        TIE_KING_OF_SPADES("There was a tie, but the winner is based on possesing the king of spades."),
        ;


        private String attributeDescription;

        WinningAttribute(String attributeDescription) {
            this.attributeDescription = attributeDescription;
        }

        public String getAttributeDescription() {
            return attributeDescription;
        }
    }

    private static final String FIELD_EMPTY_MESSAGE = "players set cannot be empty.";

    @ApiModelProperty(notes = "The winning attribute for the game.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    private WinningAttribute winningAttribute;

    @ApiModelProperty(notes = "The dealt card set to the lucky winner.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    private DealtPlayerSet WinnersSet;

    @ApiModelProperty(notes = "The dealt card set  to player two.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    private DealtPlayerSet playerSetTwo;

    @ApiModelProperty(notes = "The dealt card set to player three.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    private DealtPlayerSet playerSetThree;

    @ApiModelProperty(notes = "The dealt card set to player four.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    private DealtPlayerSet playerSetFour;


    @ApiModelProperty(notes = "The shuffled french deck of cards.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    private List<ShuffledFrenchCardDeck.Card> shuffledDeck;


    public Results() {

        DealtPlayerSet playerSet1 = new DealtPlayerSet();
        playerSet1.setPlayerName("");
        this.WinnersSet = playerSet1;

        DealtPlayerSet playerSet2 = new DealtPlayerSet();
        playerSet2.setPlayerName("");
        this.playerSetTwo = playerSet2;

        DealtPlayerSet playerSet3 = new DealtPlayerSet();
        playerSet3.setPlayerName("");
        this.playerSetThree = playerSet3;

        DealtPlayerSet playerSet4 = new DealtPlayerSet();
        playerSet4.setPlayerName("");
        this.playerSetFour = playerSet4;

    }

    public WinningAttribute getWinningAttribute() {
        return winningAttribute;
    }

    public void setWinningAttribute(WinningAttribute winningAttribute) {
        this.winningAttribute = winningAttribute;
    }

    public DealtPlayerSet getWinnersSet() {
        return WinnersSet;
    }

    public void setWinnersSet(DealtPlayerSet winnersSet) {
        WinnersSet = winnersSet;
    }

    public DealtPlayerSet getPlayerSetTwo() {
        return playerSetTwo;
    }

    public void setPlayerSetTwo(DealtPlayerSet playerSetTwo) {
        this.playerSetTwo = playerSetTwo;
    }

    public DealtPlayerSet getPlayerSetThree() {
        return playerSetThree;
    }

    public void setPlayerSetThree(DealtPlayerSet playerSetThree) {
        this.playerSetThree = playerSetThree;
    }

    public DealtPlayerSet getPlayerSetFour() {
        return playerSetFour;
    }

    public void setPlayerSetFour(DealtPlayerSet playerSetFour) {
        this.playerSetFour = playerSetFour;
    }

    public List<ShuffledFrenchCardDeck.Card> getShuffledDeck() {
        return shuffledDeck;
    }

    public void setShuffledDeck(List<ShuffledFrenchCardDeck.Card> shuffledDeck) {
        this.shuffledDeck = shuffledDeck;
    }
}
