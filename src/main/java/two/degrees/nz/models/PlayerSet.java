package two.degrees.nz.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

@ApiModel(description = "A Player's set of unique cards.")
public class PlayerSet {

    private static final String FIELD_EMPTY_MESSAGE = "players card set cannot be empty.";
    private static final String REGEX_NAME_FIELDS =           "^[^\\\\<>~!@#$%^&*()_+={}|\\[\\]:;\"',.?/']*$";
    private static final String SPECIAL_CHARACTERS_MESSAGE = "Field cannot have any of the following special characters ^[^\\\\<>~!@#$^&*()_+={}|\\[\\]:;\"'?'+]*$";

    @ApiModelProperty(notes = "Player Name.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    @Pattern(regexp = REGEX_NAME_FIELDS, message = SPECIAL_CHARACTERS_MESSAGE)
    private String playerName;

    @ApiModelProperty(notes = "The card set of player.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    private List<ShuffledFrenchCardDeck.Card> playerCardSet;

    public PlayerSet() {
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public List<ShuffledFrenchCardDeck.Card> getPlayerCardSet() {
        return playerCardSet;
    }

    public void setPlayerCardSet(List<ShuffledFrenchCardDeck.Card> playerCardSet) {
        this.playerCardSet = playerCardSet;
    }
}
