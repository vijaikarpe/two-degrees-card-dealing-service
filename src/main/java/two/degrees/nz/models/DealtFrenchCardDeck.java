package two.degrees.nz.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

@ApiModel(description = "A dealt french deck of cards amongst four players.")
public class DealtFrenchCardDeck {

    private static final String FIELD_EMPTY_MESSAGE = "players set cannot be empty.";

    @ApiModelProperty(notes = "The dealt card set to player one.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    private DealtPlayerSet playerSetOne;

    @ApiModelProperty(notes = "The dealt card set  to player two.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    private DealtPlayerSet playerSetTwo;

    @ApiModelProperty(notes = "The dealt card set to player three.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    private DealtPlayerSet playerSetThree;

    @ApiModelProperty(notes = "The dealt card set to player four.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    private DealtPlayerSet playerSetFour;

    public DealtFrenchCardDeck() {

        DealtPlayerSet playerSet1 = new DealtPlayerSet();
        playerSet1.setPlayerName("");
        this.playerSetOne = playerSet1;

        DealtPlayerSet playerSet2 = new DealtPlayerSet();
        playerSet2.setPlayerName("");
        this.playerSetTwo = playerSet2;

        DealtPlayerSet playerSet3 = new DealtPlayerSet();
        playerSet3.setPlayerName("");
        this.playerSetThree = playerSet3;

        DealtPlayerSet playerSet4 = new DealtPlayerSet();
        playerSet4.setPlayerName("");
        this.playerSetFour = playerSet4;

    }

    public DealtPlayerSet getPlayerSetOne() {
        return playerSetOne;
    }

    public void setPlayerSetOne(DealtPlayerSet playerSetOne) {
        this.playerSetOne = playerSetOne;
    }

    public DealtPlayerSet getPlayerSetTwo() {
        return playerSetTwo;
    }

    public void setPlayerSetTwo(DealtPlayerSet playerSetTwo) {
        this.playerSetTwo = playerSetTwo;
    }

    public DealtPlayerSet getPlayerSetThree() {
        return playerSetThree;
    }

    public void setPlayerSetThree(DealtPlayerSet playerSetThree) {
        this.playerSetThree = playerSetThree;
    }

    public DealtPlayerSet getPlayerSetFour() {
        return playerSetFour;
    }

    public void setPlayerSetFour(DealtPlayerSet playerSetFour) {
        this.playerSetFour = playerSetFour;
    }
}
