package two.degrees.nz.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

@ApiModel(description = "A Player's set of dealt cards.")
public class DealtPlayerSet {

    private static final String FIELD_EMPTY_MESSAGE = "players card set cannot be empty.";
    private static final String REGEX_NAME_FIELDS =           "^[^\\\\<>~!@#$%^&*()_+={}|\\[\\]:;\"',.?/']*$";
    private static final String SPECIAL_CHARACTERS_MESSAGE = "Field cannot have any of the following special characters ^[^\\\\<>~!@#$^&*()_+={}|\\[\\]:;\"'?'+]*$";

    @ApiModelProperty(notes = "Player Name.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    @Pattern(regexp = REGEX_NAME_FIELDS, message = SPECIAL_CHARACTERS_MESSAGE)
    private String playerName;

    @ApiModelProperty(notes = "The dealt card set of the player.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    private List<ShuffledFrenchCardDeck.Card> playerCardSet;

    @ApiModelProperty(notes = "The wining cards in case of a tie.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    private List<ShuffledFrenchCardDeck.Card> kingCardSet;

    @ApiModelProperty(notes = "Player Points.")
    @NotNull(message = FIELD_EMPTY_MESSAGE)
    private Integer playerPoints;

    public DealtPlayerSet() {
    }

    public List<ShuffledFrenchCardDeck.Card> getKingCardSet() {
        return kingCardSet;
    }

    public void setKingCardSet(List<ShuffledFrenchCardDeck.Card> kingCardSet) {
        this.kingCardSet = kingCardSet;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public List<ShuffledFrenchCardDeck.Card> getPlayerCardSet() {
        return playerCardSet;
    }

    public void setPlayerCardSet(List<ShuffledFrenchCardDeck.Card> playerCardSet) {
        this.playerCardSet = playerCardSet;
    }

    public Integer getPlayerPoints() {
        return playerPoints;
    }

    public void setPlayerPoints(Integer playerPoints) {
        this.playerPoints = playerPoints;
    }
}
