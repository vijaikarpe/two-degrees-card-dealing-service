package two.degrees.nz.operations;

import ch.qos.logback.classic.Level;
import two.degrees.nz.configurations.LogLevel;
import two.degrees.nz.exceptions.type.LogLevelNotFound;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@RestController
@Api(value = "/logging")
@RequestMapping("/0.0.1/logging")
public class LoggingController {

    @Value("${swaggerInfo.groupName}")
    private String GROUP_NAME;

    private  final Logger logger = LoggerFactory.getLogger(LoggingController.class);

    @ApiOperation(value = "Change the logging Level for the application.", notes = "The levels which can be set are DEBUG, INFO, WARN, ERROR, TRACE, ALL and OFF", tags = {"logging-controller"})
    @PutMapping("{logLevel}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public LogLevel entirePackage(@RequestHeader(value = "x-request-id") String correlationId, @PathVariable String logLevel) throws LogLevelNotFound {

        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(GROUP_NAME);
        root.setLevel(Level.INFO);
        switch (logLevel.toUpperCase()){

            case "DEBUG":
                logger.info("Logging set to DEBUG.");
                root.setLevel(Level.DEBUG);
                return new LogLevel(logLevel);

            case "INFO":
                logger.info("Logging set to INFO.");
                root.setLevel(Level.INFO);
                return new LogLevel(logLevel);

            case "WARN":
                logger.info("Logging set to WARN.");
                root.setLevel(Level.WARN);
                return new LogLevel(logLevel);

            case "ERROR":
                logger.info("Logging set to ERROR.");
                root.setLevel(Level.ERROR);
                return new LogLevel(logLevel);

            case "TRACE":
                logger.info("Logging set to TRACE.");
                root.setLevel(Level.TRACE);
                return new LogLevel(logLevel);

            case "ALL":
                logger.info("Logging set to ALL.");
                root.setLevel(Level.ALL);
                return new LogLevel(logLevel);

            case "OFF":
                logger.info("Logging set to OFF.");
                root.setLevel(Level.OFF);
                return new LogLevel(logLevel);

                default:
                    throw new LogLevelNotFound(logLevel, "Logging Level is not recognized.");
        }

    }

    @ApiOperation(value = "Get the logging Level for the application.", tags = {"logging-controller"})
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public LogLevel level(@RequestHeader(value = "x-request-id") String correlationId){
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(GROUP_NAME);
        return new LogLevel(root.getLevel().toString());

    }
}
