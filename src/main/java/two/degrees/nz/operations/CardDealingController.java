package two.degrees.nz.operations;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import two.degrees.nz.exceptions.type.BadRequestFailure;
import two.degrees.nz.exceptions.type.InternalFailure;
import two.degrees.nz.models.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@RestController
@Api(value = "/0.0.1")
@RequestMapping(value = "/0.0.1", produces = {"application/json"}, consumes = "application/json")
@Validated
public class CardDealingController {

    private final String EXCEPTION = "EXCEPTION: ";
    private final String STACK_TRACE = "STACK TRACE: ";
    private final String NEW_LINE = "\n";
    private final String EMPTY = "";
    private final String BAD_AUTH_KEY = "BAD AUTH KEY";
    private final String BAD_REQUEST_BODY = "Bad Request Body";
    private final Logger logger = LoggerFactory.getLogger(CardDealingController.class);

    private String stackTraceToString(Throwable e) {
        StringBuilder sb = new StringBuilder();
        sb.append(EXCEPTION + e.toString() + NEW_LINE + STACK_TRACE + NEW_LINE);
        for (StackTraceElement element : e.getStackTrace()) {
            sb.append(element.toString());
            sb.append(NEW_LINE);
        }
        return sb.toString();
    }


    @Value("${spring.application.name}")
    private String DATA_SOURCE_USER_NAME;

    @ApiOperation(value = "Deal each Player's Card Set.", notes = "Remove the lowest card from each suite and return the points for each player.", tags = {"card-dealing-controller"}, authorizations = @Authorization(value = "Bearer"))
    @PostMapping("/deal")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @ResponseBody
    public DealtFrenchCardDeck getDealtDistributedFrenchCardDeck(@Valid @RequestBody DistributedFrenchCardDeck distributedFrenchCardDeck, @NotEmpty @NotBlank @NotNull @RequestHeader(value = "auth-key", required = true) String authId, @RequestHeader(value = "x-request-id", required = true) String correlationId) throws RuntimeException {

        try {

            if(authId.equals(DATA_SOURCE_USER_NAME)){

                if (distributedFrenchCardDeck.getShuffledDeck()!=null &&
                        distributedFrenchCardDeck.getPlayerSetFour().getPlayerCardSet()!=null &&
                distributedFrenchCardDeck.getPlayerSetThree().getPlayerCardSet() != null &&
                distributedFrenchCardDeck.getPlayerSetTwo().getPlayerCardSet() != null &&
                distributedFrenchCardDeck.getPlayerSetOne().getPlayerCardSet() != null){


                    //Always sort the cards to prevent any fault injections from the down stream sorter service (adds redundancy, but still a good trade off)

                    DistributedFrenchCardDeck dealtDeck = new DistributedFrenchCardDeck();
                    List<ShuffledFrenchCardDeck.Card> sortedListPlayerOne = getSortedCardsForPlayer(distributedFrenchCardDeck.getPlayerSetOne().getPlayerCardSet());
                    List<ShuffledFrenchCardDeck.Card> dealtListPlayerOne = getDealtCardsForPlayer(sortedListPlayerOne);
                    PlayerSet playerSetOne = new PlayerSet();
                    playerSetOne.setPlayerName(distributedFrenchCardDeck.getPlayerSetOne().getPlayerName());
                    playerSetOne.setPlayerCardSet(dealtListPlayerOne);
                    dealtDeck.setPlayerSetOne(playerSetOne);

                    List<ShuffledFrenchCardDeck.Card> sortedListPlayerTwo = getSortedCardsForPlayer(distributedFrenchCardDeck.getPlayerSetTwo().getPlayerCardSet());
                    List<ShuffledFrenchCardDeck.Card> dealtListPlayerTwo = getDealtCardsForPlayer(sortedListPlayerTwo);
                    PlayerSet playerSetTwo = new PlayerSet();
                    playerSetTwo.setPlayerName(distributedFrenchCardDeck.getPlayerSetTwo().getPlayerName());
                    playerSetTwo.setPlayerCardSet(dealtListPlayerTwo);
                    dealtDeck.setPlayerSetTwo(playerSetTwo);

                    List<ShuffledFrenchCardDeck.Card> sortedListPlayerThree = getSortedCardsForPlayer(distributedFrenchCardDeck.getPlayerSetThree().getPlayerCardSet());
                    List<ShuffledFrenchCardDeck.Card> dealtListPlayerThree = getDealtCardsForPlayer(sortedListPlayerThree);
                    PlayerSet playerSetThree = new PlayerSet();
                    playerSetThree.setPlayerName(distributedFrenchCardDeck.getPlayerSetThree().getPlayerName());
                    playerSetThree.setPlayerCardSet(dealtListPlayerThree);
                    dealtDeck.setPlayerSetThree(playerSetThree);

                    List<ShuffledFrenchCardDeck.Card> sortedListPlayerFour = getSortedCardsForPlayer(distributedFrenchCardDeck.getPlayerSetFour().getPlayerCardSet());
                    List<ShuffledFrenchCardDeck.Card> dealtListPlayerFour = getDealtCardsForPlayer(sortedListPlayerFour);
                    PlayerSet playerSetFour = new PlayerSet();
                    playerSetFour.setPlayerName(distributedFrenchCardDeck.getPlayerSetFour().getPlayerName());
                    playerSetFour.setPlayerCardSet(dealtListPlayerFour);
                    dealtDeck.setPlayerSetFour(playerSetFour);

                    dealtDeck.setShuffledDeck(distributedFrenchCardDeck.getShuffledDeck());

                    DealtFrenchCardDeck dealtFrenchCardDeck = new DealtFrenchCardDeck();

                    DealtPlayerSet dealtPlayerSetFirst = new DealtPlayerSet();
                    dealtPlayerSetFirst.setPlayerName(distributedFrenchCardDeck.getPlayerSetOne().getPlayerName());
                    dealtPlayerSetFirst.setPlayerCardSet(dealtDeck.getPlayerSetOne().getPlayerCardSet());
                    dealtPlayerSetFirst.setPlayerPoints(getScoreForPlayer(dealtDeck.getPlayerSetOne().getPlayerCardSet()));
                    dealtPlayerSetFirst.setKingCardSet(getKingCardSetForPlayer(dealtDeck.getPlayerSetOne().getPlayerCardSet()));
                    dealtFrenchCardDeck.setPlayerSetOne(dealtPlayerSetFirst);

                    DealtPlayerSet dealtPlayerSetSecond = new DealtPlayerSet();
                    dealtPlayerSetSecond.setPlayerName(distributedFrenchCardDeck.getPlayerSetTwo().getPlayerName());
                    dealtPlayerSetSecond.setPlayerCardSet(dealtDeck.getPlayerSetTwo().getPlayerCardSet());
                    dealtPlayerSetSecond.setPlayerPoints(getScoreForPlayer(dealtDeck.getPlayerSetTwo().getPlayerCardSet()));
                    dealtPlayerSetSecond.setKingCardSet(getKingCardSetForPlayer(dealtDeck.getPlayerSetTwo().getPlayerCardSet()));
                    dealtFrenchCardDeck.setPlayerSetTwo(dealtPlayerSetSecond);

                    DealtPlayerSet dealtPlayerSetThird = new DealtPlayerSet();
                    dealtPlayerSetThird.setPlayerName(distributedFrenchCardDeck.getPlayerSetThree().getPlayerName());
                    dealtPlayerSetThird.setPlayerCardSet(dealtDeck.getPlayerSetThree().getPlayerCardSet());
                    dealtPlayerSetThird.setPlayerPoints(getScoreForPlayer(dealtDeck.getPlayerSetThree().getPlayerCardSet()));
                    dealtPlayerSetThird.setKingCardSet(getKingCardSetForPlayer(dealtDeck.getPlayerSetThree().getPlayerCardSet()));
                    dealtFrenchCardDeck.setPlayerSetThree(dealtPlayerSetThird);

                    DealtPlayerSet dealtPlayerSetFourth = new DealtPlayerSet();
                    dealtPlayerSetFourth.setPlayerName(distributedFrenchCardDeck.getPlayerSetFour().getPlayerName());
                    dealtPlayerSetFourth.setPlayerCardSet(dealtDeck.getPlayerSetFour().getPlayerCardSet());
                    dealtPlayerSetFourth.setPlayerPoints(getScoreForPlayer(dealtDeck.getPlayerSetFour().getPlayerCardSet()));
                    dealtPlayerSetFourth.setKingCardSet(getKingCardSetForPlayer(dealtDeck.getPlayerSetFour().getPlayerCardSet()));
                    dealtFrenchCardDeck.setPlayerSetFour(dealtPlayerSetFourth);


                    return dealtFrenchCardDeck;

                }else{

                    throw new BadRequestFailure(EMPTY, BAD_REQUEST_BODY);
                }

            }else {
                throw new BadRequestFailure(EMPTY, BAD_AUTH_KEY);
            }

        }catch (BadRequestFailure e){
            throw e;
        }

        catch (Exception e) {
            throw new InternalFailure(stackTraceToString(e));
        }
    }

    private List<ShuffledFrenchCardDeck.Card> getDealtCardsForPlayer(List<ShuffledFrenchCardDeck.Card> playersCardSet){

        ArrayList<ShuffledFrenchCardDeck.Card> diamonds = new ArrayList<>();
        List<ShuffledFrenchCardDeck.Card> hearts = new ArrayList<>();
        List<ShuffledFrenchCardDeck.Card> spades = new ArrayList<>();
        ArrayList<ShuffledFrenchCardDeck.Card> clubs = new ArrayList<>();

        for (ShuffledFrenchCardDeck.Card card : playersCardSet) {

            switch (card.getSuiteType()){

                case HEARTS:
                    hearts.add(card);
                    break;
                case DIAMONDS:
                    diamonds.add( card);
                    break;
                case SPADES:
                    spades.add(card);
                    break;
                case CLUBS:
                    clubs.add(card);
                    break;

                default:
                    throw new BadRequestFailure("Card Suite Type undetected " + card.getSuiteType().toString(), "Card Suite Type undetected " + card.getSuiteType().toString());
            }

        }

        if (hearts.size()>0){
            hearts.remove(0);
        }

        if (diamonds.size()>0){
            diamonds.remove(0);
        }


        if (clubs.size()>0){
            clubs.remove(0);
        }

        if (spades.size()>0){
            spades.remove(0);
        }


        return Stream.of(clubs, diamonds, hearts, spades)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private List<ShuffledFrenchCardDeck.Card> getSortedCardsForPlayer(List<ShuffledFrenchCardDeck.Card> playersCardSet){

        ArrayList<ShuffledFrenchCardDeck.Card> diamonds = new ArrayList<>(Collections.nCopies(52, ShuffledFrenchCardDeck.Card.JOKER));
        List<ShuffledFrenchCardDeck.Card> hearts = new ArrayList<>(Collections.nCopies(52, ShuffledFrenchCardDeck.Card.JOKER));
        List<ShuffledFrenchCardDeck.Card> spades = new ArrayList<>(Collections.nCopies(52, ShuffledFrenchCardDeck.Card.JOKER));
        ArrayList<ShuffledFrenchCardDeck.Card> clubs = new ArrayList<>(Collections.nCopies(52, ShuffledFrenchCardDeck.Card.JOKER));

        for (ShuffledFrenchCardDeck.Card card : playersCardSet) {

            switch (card.getSuiteType()){

                case HEARTS:
                    hearts.set(card.getCardValue(), card);
                    break;
                case DIAMONDS:
                    diamonds.set(card.getCardValue(), card);
                    break;
                case SPADES:
                    spades.set(card.getCardValue(), card);
                    break;
                case CLUBS:
                    clubs.set(card.getCardValue(), card);
                    break;

                default:
                    throw new BadRequestFailure("Card Suite Type undetected " + card.getSuiteType().toString(), "Card Suite Type undetected " + card.getSuiteType().toString());
            }

        }

        hearts.removeAll(Collections.singleton(ShuffledFrenchCardDeck.Card.JOKER));
        diamonds.removeAll(Collections.singleton(ShuffledFrenchCardDeck.Card.JOKER));
        spades.removeAll(Collections.singleton(ShuffledFrenchCardDeck.Card.JOKER));
        clubs.removeAll(Collections.singleton(ShuffledFrenchCardDeck.Card.JOKER));

        return Stream.of(clubs, diamonds, hearts, spades)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private List<ShuffledFrenchCardDeck.Card> getKingCardSetForPlayer(List<ShuffledFrenchCardDeck.Card> cardSet){

        List<ShuffledFrenchCardDeck.Card> kingCards = new ArrayList<>();

        for (ShuffledFrenchCardDeck.Card card : cardSet) {

            switch (card){

                case CLUBS_KING:
                    kingCards.add(card);
                    break;

                case DIAMONDS_KING:
                    kingCards.add(card);
                    break;

                case HEARTS_KING:
                    kingCards.add(card);
                    break;

                case SPADES_KING:
                    kingCards.add(card);
                    break;

            }
        }
        return kingCards;
    }

    private Integer getScoreForPlayer(List<ShuffledFrenchCardDeck.Card> cardSet){

        Integer score = 0;

        for (ShuffledFrenchCardDeck.Card card : cardSet) {
            score += card.getCardValue();
        }

        return score;
    }
}
