package two.degrees.nz.exceptions.type;

public class ContinuousDeclineResponse {
    public ContinuousDeclineResponse()  {
    }

    private String message;

    public String getProfitability() {
        return profitability;
    }

    public void setProfitability(String profitability) {
        this.profitability = profitability;
    }

    private String profitability;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
