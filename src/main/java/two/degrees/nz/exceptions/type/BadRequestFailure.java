package two.degrees.nz.exceptions.type;

public class BadRequestFailure extends RuntimeException {
    public BadRequestFailure(String message, String clientMessage) {
        super(message);
        this.clientMessage = clientMessage;
    }

    private String clientMessage;

    public String getClientMessage() {
        return clientMessage;
    }

    public void setClientMessage(String clientMessage) {
        this.clientMessage = clientMessage;
    }
}


