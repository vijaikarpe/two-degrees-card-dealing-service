package two.degrees.nz.exceptions.type;

public class InvalidInputResponse {

    private String errorMessage;

    public InvalidInputResponse() {
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
