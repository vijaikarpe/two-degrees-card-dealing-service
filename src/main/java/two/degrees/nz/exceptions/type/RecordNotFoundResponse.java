package two.degrees.nz.exceptions.type;

public class RecordNotFoundResponse {
    public RecordNotFoundResponse()  {
    }

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
