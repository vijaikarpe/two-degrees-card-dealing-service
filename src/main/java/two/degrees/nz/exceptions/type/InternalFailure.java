package two.degrees.nz.exceptions.type;

public class InternalFailure extends RuntimeException {
    public InternalFailure(String message) {
        super(message);
    }
}


