package two.degrees.nz.exceptions.type;

public class ContinuousDecline extends RuntimeException {
    public ContinuousDecline(String message) {
        super(message);
    }
}



