package two.degrees.nz.exceptions;

import two.degrees.nz.exceptions.type.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Component
@ControllerAdvice
public class ExceptionHandlingController {


    @Value("${responseMessages.internalError}")
    private String INTERNAL_SERVER_RESPONSE_MESSAGE;

    private  final Logger logger = LoggerFactory.getLogger(ExceptionHandlingController.class);

    @ExceptionHandler(BadRequestFailure.class)
    public ResponseEntity<BadRequestFailureResponse> badRequestFailure(BadRequestFailure ex) {
        logger.info(ex.getMessage());
        BadRequestFailureResponse response = new BadRequestFailureResponse();
        response.setMessage(ex.getClientMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InternalFailure.class)
    public ResponseEntity<InternalFailureResponse> internalFailure(InternalFailure ex) {
        logger.error(ex.getMessage());
        InternalFailureResponse response = new InternalFailureResponse();
        response.setMessage(INTERNAL_SERVER_RESPONSE_MESSAGE);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidInput.class)
    public ResponseEntity<InvalidInputResponse> invalidInput(InvalidInput ex) {
        InvalidInputResponse response = new InvalidInputResponse();
        response.setErrorMessage(ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ContinuousDecline.class)
    public ResponseEntity<ContinuousDeclineResponse> continuousDecline(ContinuousDecline cd) {
        ContinuousDeclineResponse continuousDeclineResponse = new ContinuousDeclineResponse();
        continuousDeclineResponse.setMessage(cd.getMessage());
        continuousDeclineResponse.setProfitability("NONE");
        return new ResponseEntity<>(continuousDeclineResponse, HttpStatus.OK);
    }

    @ExceptionHandler(LogLevelNotFound.class)
    public ResponseEntity<LogLevelNotFound> logLevelNotFound(LogLevelNotFound ex) {
        LogLevelNotFoundResponse response = new LogLevelNotFoundResponse();
        response.setErrorMessage(ex.getMessage());
        response.setLogLevel(ex.getLoglevel());
        logger.info(ex.getMessage()+ " " + ex.getLoglevel());
        return new ResponseEntity(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(RecordNotFound.class)
    public ResponseEntity<RecordNotFound> recordNotFound(RecordNotFound ex) {
        RecordNotFoundResponse response = new RecordNotFoundResponse();
        response.setMessage(ex.getClientMessage());
        logger.info(ex.getMessage()+ " " + ex.getMessage());
        return new ResponseEntity(response, HttpStatus.NOT_FOUND);
    }
}
