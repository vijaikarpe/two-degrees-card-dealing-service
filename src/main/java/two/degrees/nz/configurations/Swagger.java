package two.degrees.nz.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Component
@Configuration
@EnableSwagger2
public class Swagger {

    //region SWAGGER INFO
    @Value("${swaggerInfo.groupName}")
    private String GROUP_NAME;

    @Value("${swaggerInfo.basePackage}")
    private String BASE_PACKAGE;

    @Value("${swaggerInfo.title}")
    private String TITLE;

    @Value("${swaggerInfo.description}")
    private String DESCRIPTION;

    @Value("${swaggerInfo.version}")
    private String VERSION;

    @Value("${swaggerInfo.termsOfService}")
    private String TERMS_OF_SERVICE;

    @Value("${swaggerInfo.contactDetails.name}")
    private String CONTACT_NAME;

    @Value("${swaggerInfo.contactDetails.url}")
    private String CONTACT_URL;

    @Value("${swaggerInfo.contactDetails.email}")
    private String CONTACT_EMAIL;

    @Value("${swaggerInfo.license.terms}")
    private String LICENSE_TERMS;

    @Value("${swaggerInfo.license.url}")
    private String LICENSE_URL;
    //endregion

    @ConditionalOnMissingBean
    @Bean
    public Docket domainApi(){

        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(GROUP_NAME)
                .select()
                .apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(metaInfo());
    }

    private ApiInfo metaInfo() {

        ApiInfo apiInfo = new ApiInfo(TITLE, DESCRIPTION, VERSION, TERMS_OF_SERVICE, new Contact(CONTACT_NAME, CONTACT_URL, CONTACT_EMAIL), LICENSE_TERMS, LICENSE_URL, Collections.emptyList());
        return apiInfo;
    }

}
